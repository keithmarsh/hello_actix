FROM ubuntu:22.04
 
RUN apt-get update && apt-get install -y curl 
RUN apt-get install build-essential -y
 
COPY target/debug/hello_actix target/debug/
COPY static static/
EXPOSE 8080
CMD target/debug/hello_actix